from area_finder import square_area, triangle_area

def test_square_area_1():
    assert(square_area(1) == 1)

def test_square_area_2():
    assert(square_area(2) == 4)

def test_square_area_3():
    assert(square_area(3) == 9)

def test_square_area_4():
    assert(square_area(0) == 0)


def test_triangle_area_5():
    assert(triangle_area(0, 0) == 0)

def test_triangle_area_6():
    assert(triangle_area(4, 2) == 4)
