# GitLab's Code Quality Scanner (Python example)

## The following code quality issues are shown in the Merge Requests
- [Code complexity](https://gitlab.com/gitlab-learn-labs/webinars/security/code-quality-scanner/-/merge_requests/1)
- [Code duplication](https://gitlab.com/gitlab-learn-labs/webinars/security/code-quality-scanner/-/merge_requests/3)
- [Arguments above a threshold](https://gitlab.com/gitlab-learn-labs/webinars/security/code-quality-scanner/-/merge_requests/4)

## Notes
This was from a [Python Code Quality Project](https://gitlab.com/releases-demos/python-code-quality) by [@warias](https://gitlab.com/warias) showing code complexity (video [here](https://www.youtube.com/watch?v=DwfksFO16c4&list=PLFGfElNsQthYDx0A_FaNNfUm9NHsK6zED&index=86)), which was modified to include issues related to code duplication and arguments above a threshold. 
